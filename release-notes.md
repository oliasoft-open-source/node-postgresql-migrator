# Node PostgreSQL Migrator Release Notes

## 2.4.1

- Minor upgrades to internal dependencies + test improvements ([OW-20204](https://oliasoft.atlassian.net/browse/OW-20204))

## 2.4.0

- Upgrade all internal dependencies and upgrade codebase to TypeScript ([OW-20204](https://oliasoft.atlassian.net/browse/OW-20204))

## 2.3.5

- Upgrade to Node version 22 ([OW-18912](https://oliasoft.atlassian.net/browse/OW-18912))

## 2.3.4

- Fix mattermost url and changed channel name to Release bots [OW-14762](https://oliasoft.atlassian.net/browse/OW-14762)

## 2.3.3

- Fix release CI/CD pipeline ([OW-11691](https://oliasoft.atlassian.net/browse/OW-11691))

## 2.3.2

- handle build step in CI/CD pipelines instead of committing `dist` files ([OW-11009](https://oliasoft.atlassian.net/browse/OW-11009))

## 2.3.1

- fix release CI pipeline

## 2.3.0

- switch from npm to yarn (to match other repos)
- Add optional CI pipeline that automates publishing of beta releases ([OW-10003](https://oliasoft.atlassian.net/browse/OW-10003))

## 2.2.0

- first publish of release notes
