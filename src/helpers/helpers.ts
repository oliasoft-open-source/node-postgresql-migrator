import * as crypto from 'crypto';

export const hash = (input: string) =>
  crypto.createHash('sha256').update(input).digest('hex');

export const exit = (code: number) => process.exit(code);
