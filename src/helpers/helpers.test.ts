import { describe, mock, test } from 'node:test';
import assert from 'node:assert';
import { hash, exit } from './helpers.ts';

const validSHA = /^[a-f0-9]{64}$/gi;

describe('Helpers', () => {
  test(' hash - generates a valid SHA256 hash', async () => {
    const sha = hash('monkeys like bananas');
    assert.strictEqual(validSHA.test(sha), true);
  });

  test('exit - exits the process with exit code', async () => {
    const exitMock = mock.method(process, 'exit', (code: number) => {
      throw new Error(`process.exit() was called with code ${code}.`);
    });
    assert.throws(() => {
      exit(1);
    }, /process.exit\(\) was called with code 1\./);
    exitMock.mock.restore();
  });
});
