import { zipWith } from 'lodash-es';
import { glob } from 'glob';
import path from 'path';
import { promises as fs } from 'fs';
import { hash } from '../helpers/helpers.ts';
import { validateFileName } from '../validator/validator.ts';

const matchSkip = /.skip.sql$/g;

/**
 * Format a SQL migration file timestamp to ISO 8601
 */
export const formatFileTimeStamp = (timeStampString: string) => {
  //2020-11-26T101010 -> 2020-11-26T10:10:10Z
  const withColons = [...timeStampString]
    .map((c, i) => ([13, 15].includes(i) ? `:${c}` : c))
    .join('');
  return `${withColons}Z`; //needs to be UTC
};

/**
 * Get the timestamp from a SQL migration filename
 *
 */
export const fileNameToTimeStamp = (fileName: string) => {
  const timeStampRegex = /(\d{4})-(\d{2})-(\d{2})T(\d{2})(\d{2})(\d{2})/;
  const timeStampMatch = fileName.match(timeStampRegex);
  const timeStampString = timeStampMatch?.length ? timeStampMatch[0] : null;
  return timeStampString?.length
    ? Date.parse(formatFileTimeStamp(timeStampString))
    : null;
};

export const getFileMigrations = async (directory: string) => {
  const filePaths = (await glob(`${directory}/**/*.sql`)).sort();
  const scripts = await Promise.all(
    filePaths.map(async (file) => fs.readFile(file, 'utf-8')),
  );
  const filePathsWithSkipMeta = filePaths.map((file) => {
    return {
      skip: file.match(matchSkip) !== null,
      //consider the filename unchanged for input validation
      file: file.replace(matchSkip, '.sql'),
    };
  });
  return zipWith(
    filePathsWithSkipMeta,
    scripts,
    ({ file, skip }, rawScript) => {
      const fileName = path.parse(file).base;
      const timeStamp = fileNameToTimeStamp(fileName);
      const isValidFileName = validateFileName(fileName, timeStamp);
      const newFileHash = hash(rawScript);
      const isOnceOnly = fileName.endsWith('.once.sql');
      const insertMigration = `
          INSERT INTO migrations (file_name, file_hash)
          VALUES ('${fileName}', '${newFileHash}')
      `;
      const script = `
      ${rawScript}
      ${insertMigration}
    `;
      const fileWithMetadata = {
        fileName,
        timeStamp,
        isValidFileName,
        newFileHash,
        script,
        isOnceOnly,
        skip,
      };
      return fileWithMetadata;
    },
  );
};
