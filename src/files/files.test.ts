import { describe, test } from 'node:test';
import assert from 'node:assert';
import {
  getFileMigrations,
  formatFileTimeStamp,
  fileNameToTimeStamp,
} from './files.ts';

describe('Files', () => {
  test('formatTimeStamp - gets the current time in format 2020-11-26T101010', async () => {
    const fileTimeStamp = '2020-11-26T101010';
    const isoTimestamp = formatFileTimeStamp(fileTimeStamp);
    assert.strictEqual(typeof isoTimestamp, 'string');
    assert.strictEqual(isoTimestamp, '2020-11-26T10:10:10Z');
  });

  test('fileNameToTimeStamp - gets timestamp from filename', async () => {
    const fileName = '2020-11-26T101010-foo-bar.js';
    const timeStamp = fileNameToTimeStamp(fileName);
    assert.strictEqual(typeof timeStamp, 'number');
    assert.strictEqual(timeStamp, 1606385410000);
  });

  test('fileNameToTimeStamp - returns null for malformed filename', async () => {
    const fileName = '202-11-26T101010-foo--bar.js';
    const timeStamp = fileNameToTimeStamp(fileName);
    assert.strictEqual(timeStamp, null);
  });

  test('getFileMigrations - loads SQL migration files from a directory', async () => {
    const files = await getFileMigrations(
      './src/__tests__/__testdata__/migrations',
    );
    assert.strictEqual(files.length, 3);
    assert.strictEqual(
      files[0].fileName,
      '2021-09-07T154500-create-animals-table.sql',
    );
    assert.strictEqual(
      files[0].script.includes('CREATE TABLE IF NOT EXISTS animals'),
      true,
    );
    assert.strictEqual(
      files[1].script.includes(`INSERT INTO animals(name) VALUES('Aardvark')`),
      true,
    );
    assert.strictEqual(
      files[2].script.includes(`INSERT INTO animals(name) VALUES('Rhino')`),
      true,
    );
    assert.strictEqual(typeof files[0].newFileHash, 'string');
  });
});
