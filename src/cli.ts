import chalk from 'chalk';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import { migrator } from './migrator/migrator.ts';
import { exit } from './helpers/helpers.ts';

const args = yargs(hideBin(process.argv))
  .scriptName('migrator')
  .version(false)
  .option('db', {
    type: 'string',
    require: true,
    description:
      'PostgreSQL connection string, e.g. postgres://user:pass@host:port/db',
  })
  .option('dir', {
    type: 'string',
    require: true,
    description:
      'Path from project root to migrations directory, e.g. ./src/server/db/migrations',
  })
  .option('force', {
    alias: 'f',
    type: 'boolean',
    default: false,
    description: 'Force modified change scripts to be re-executed',
  })
  .help('help')
  .alias('help', 'h').argv;

Promise.resolve(args).then((resolvedArgs) => {
  const { db: dbConnection, dir, force } = resolvedArgs;

  migrator({ dbConnection, dir, force })
    .then(() => {
      exit(0);
    })
    .catch((error) => {
      console.log(chalk.red(error));
      exit(1);
    });
});
