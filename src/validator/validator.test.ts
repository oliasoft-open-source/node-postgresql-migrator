import { describe, test } from 'node:test';
import assert from 'node:assert';
import {
  validateFileNameFormat,
  validateTimeStamp,
  validateFileName,
} from './validator.ts';

describe('Validator', () => {
  test('validateFileNameFormat - checks filename format is valid', async () => {
    assert.strictEqual(
      validateFileNameFormat('2021-12-03T140032-description-text.sql'),
      true,
    );
    assert.strictEqual(
      validateFileNameFormat('2021-12-03T140032-more-description-test.sql'),
      true,
    );
    assert.strictEqual(
      validateFileNameFormat(
        '2021-12-03T140032-more-description-test.once.sql',
      ),
      true,
    );
    assert.strictEqual(
      validateFileNameFormat(
        '2021-12-03T140032-more-description-test.skip.sql',
      ),
      true,
    );
    assert.strictEqual(
      validateFileNameFormat(
        '2021-12-03T140032-more-description-test.once.skip.sql',
      ),
      false,
    );
    assert.strictEqual(
      validateFileNameFormat('2021-12-03T140032-init.sql'),
      true,
    );
    assert.strictEqual(
      validateFileNameFormat('2021-12-03T140032-more-description-test.js'),
      false,
    );
    assert.strictEqual(
      validateFileNameFormat('202-12-03T140032-description-text.sql'),
      false,
    );
    assert.strictEqual(
      validateFileNameFormat('2021-12-03Z140032-description-text.sql'),
      false,
    );
    assert.strictEqual(
      validateFileNameFormat('2021-12-03T140032description-text.sql'),
      false,
    );
    assert.strictEqual(
      validateFileNameFormat('2021-12-03T140032-description-text.js'),
      false,
    );
    assert.strictEqual(
      validateFileNameFormat('2021-12-03T140032-description---text.sql'),
      false,
    );
    assert.strictEqual(
      validateFileNameFormat('2021-12-03T140032-description--again-text-.sql'),
      false,
    );
  });

  test('validateTimeStamp - checks timestamp is valid', async () => {
    assert.strictEqual(validateTimeStamp(null), false);
    // @ts-expect-error intentional test of invalid type input
    assert.strictEqual(validateTimeStamp('foobar'), false);
    assert.strictEqual(validateTimeStamp(1606385410000), true);
  });

  test('validateFileName - true if fileName is valid', () => {
    const fileName = '2021-12-03T140032-description-text.sql';
    const timeStamp = 123;
    assert.strictEqual(validateFileName(fileName, timeStamp), true);
  });

  test('validateFileName - false if fileName is invalid', () => {
    const fileName = '202-12-03T140032-description-text.sql';
    const timeStamp = 123;
    assert.strictEqual(validateFileName(fileName, timeStamp), false);
  });
});
