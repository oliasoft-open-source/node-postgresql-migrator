/**
 * Checks if timestamp is valid
 */
export const validateTimeStamp = (timeStamp: number): boolean =>
  new Date(timeStamp).getTime() > 0;

/**
 * Checks if fileName format is valid
 */
export const validateFileNameFormat = (fileName: string): boolean => {
  // test cases: https://regex101.com/r/brcP5c/1
  const validFileName =
    /^(\d{4}-\d{2}-\d{2}T\d{6}-)([a-z]+|(([a-z]+-)+[a-z]+))((.sql)|(.once.sql)|(.skip.sql))$/g;
  return validFileName.test(fileName);
};

// allow some malformed filenames from before input validation was added
const allowedBadFileNames: string[] = [
  '2020-02-24-link-cubic-meters-per-time-to-flowrate.sql',
  '2020-03-13-T103000-BHA-Components-columns-cleanup.sql',
  '2020-03-18T145000-Grades-columns-cleanup.sql',
  '2020-04-07T130000-Cleanup-rheologies-columns.sql',
  '2020-04-09T110000_add-country-column-in-datasets.sql',
  '2020-05-015000000-new-gas-density-units-and-quantities.sql',
  '2020-05-05T000000-fix-msm3-unit-label.sql',
  '2020-05-12T000000-update-blowout-gas_oil-flowrate-default-unit-ncs.sql',
  '2020-05-15T000000-add-unit-klbf_and_misc_sync.sql',
  '2020-05-26T100000_add-status-column-in-datasets.sql',
  '2020-06-245000000-new-oil-density-quantity.sql',
  '2020-07-03T060000-alter-rheologies-destructurize-direct-input-to-columns_Rev1revert.sql',
  '2020-07-03T060000-alter-rheologies-destructurize-direct-input-to-columns_Rev2.sql',
  '2020-07-14T142744_add-accessids-column-in-datasets.sql',
  '2020-08-21T152700-connections-hasEnvelope-flag-column.sql',
  '2020-09-11T130000-Hydrocarbons-remove-temp-input-liq-density-references.sql',
  '2020-10-19-add-quantities-for-ksi-klbf.sql',
  '2020-10-19-fix-most-remaining-missing-unit-quantity-mappings.sql',
  '2020-11-23T224600_add-created-timestamp.sql',
  '2021-03-16T0162100-drilling-fluids-drop-fluid-types-synth-mud-compositions.sql',
  '2021-03-23-T150000-remove-null-empty-sort-values-dataset.sql',
  '2021-05-07-fix-unit-descrption.sql',
  '2021-05-12T094400-add-gpm-unit-to-pumpRate-quantity.sql',
  '2021-07-08T152211-add-on-delete-pecker-packer_envelopes_point-derations_point_set.sql',
  '2021-08-09T150700-hotfix_hierarchy.sql',
  '2021-26-01T090000-create-auditlog.sql',
  '2021-28-02T210000-change-fk-constraints.sql',
  '2021-04-23T121200-Hydrocarbons-components-pvt-refactor.sql',
  '2021-09-14-hydrocarbons-GOR.sql',
  '2021-09-13T130000-add-deg-per-10m-unit-in-dogleg-severity-unit.sql',
];

/**
 * Checks if a file metadata is valid and throws if not
 */
export const validateFileName = (
  fileName: string,
  timeStamp: number,
): boolean => {
  return (
    allowedBadFileNames.includes(fileName) ||
    (validateFileNameFormat(fileName) && validateTimeStamp(timeStamp))
  );
};
