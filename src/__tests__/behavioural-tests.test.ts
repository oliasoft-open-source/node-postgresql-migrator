import { mock, test, beforeEach, afterEach, describe } from 'node:test';
import assert from 'node:assert';
import { newDb } from 'pg-mem';
import { migrator } from '../migrator/migrator.ts';
import DBModule, { createMigrationsTable } from '../db/db.ts';

let db;
const dbConnection = 'postgres://username:password@localhost:5432/mockedDb'; // doesn't matter, mocked anyway
const testMigrationsDirectory = './src/__tests__/__testdata__/migrations';
const alteredMigrationsDirectory =
  './src/__tests__/__testdata__/altered-migrations';
const emptyMigrationsDirectory =
  './src/__tests__/__testdata__/empty-migrations';
const malformFileNameMigrationsDirectory =
  './src/__tests__/__testdata__/malformed-filename-migrations';
const malformedSQLMigrationsDirectory =
  './src/__tests__/__testdata__/malformed-sql-migrations';
const skippedMigrationsDirectory =
  './src/__tests__/__testdata__/skipped-migrations';

let dbMock;

describe('Behavioural smoke tests', () => {
  beforeEach(async () => {
    db = await newDb().adapters.createPgPromise();
    db.connect();
    await createMigrationsTable(db);
    dbMock = mock.method(DBModule, 'getDbConnection', () => {
      return db;
    });
  });
  afterEach(() => {
    dbMock.mock.restore();
  });

  test('can invoke migrator function', async () => {
    await migrator({
      dbConnection,
      dir: testMigrationsDirectory,
      autoInitializeTable: false,
    });
    const animals = await db.any('SELECT * FROM animals');
    assert.strictEqual(animals.length, 4);
    assert.strictEqual(animals[0].name, 'Alpaca');
    assert.strictEqual(animals[3].name, 'Rhino');
  });

  test('can invoke migrator function twice without errors', async () => {
    await migrator({
      dbConnection,
      dir: testMigrationsDirectory,
      autoInitializeTable: false,
    });
    await migrator({
      dbConnection,
      dir: testMigrationsDirectory,
      autoInitializeTable: false,
    });
    const animals = await db.any('SELECT * FROM animals');
    assert.strictEqual(animals.length, 4);
    assert.strictEqual(animals[0].name, 'Alpaca');
    assert.strictEqual(animals[3].name, 'Rhino');
  });

  test('force executes altered migration scripts, except for *.once.sql', async () => {
    await migrator({
      dbConnection,
      dir: testMigrationsDirectory,
      autoInitializeTable: false,
    });
    await migrator({
      dbConnection,
      dir: alteredMigrationsDirectory,
      force: true,
      autoInitializeTable: false,
    });
    const animals = await db.any('SELECT * FROM animals');
    assert.strictEqual(animals.length, 6);
    assert.strictEqual(animals[0].name, 'Alpaca');
    assert.strictEqual(animals[5].name, 'Water buffalo');
    assert.strictEqual(
      animals.some((animal) => animal.name === 'Zebras'),
      false,
    );
  });

  test('migrator function when there are no migrations', async () => {
    const consoleSpy = mock.method(console, 'log');
    await migrator({
      dbConnection,
      dir: emptyMigrationsDirectory,
      autoInitializeTable: false,
    });
    assert.match(
      consoleSpy.mock.calls[0].arguments[0],
      /No new migrations found/,
    );
    consoleSpy.mock.restore();
  });

  test('migrator function throws upon invalid input', async () => {
    await assert.rejects(
      migrator({
        dbConnection,
        dir: malformFileNameMigrationsDirectory,
        autoInitializeTable: false,
      }),
      new Error(
        'Some filenames have invalid format (ISO 8601 without colons e.g. 2021-09-07T093045-some-description.sql)',
      ),
    );
  });

  test('migrator function throws upon invalid input', async () => {
    await assert.rejects(
      migrator({
        dbConnection,
        dir: malformedSQLMigrationsDirectory,
        autoInitializeTable: false,
      }),
      (err: Error) => {
        assert.strictEqual(
          err.message.includes('Your query failed to parse'),
          true,
        );
        return true;
      },
    );
  });

  test('does not execute scripts that have been renamed to *.skip.sql', async () => {
    await migrator({
      dbConnection,
      dir: skippedMigrationsDirectory,
      autoInitializeTable: false,
    });
    const animals = await db.any('SELECT * FROM animals');
    assert.strictEqual(animals.length, 3);
    assert.strictEqual(animals[0].name, 'Alpaca');
    assert.strictEqual(animals[2].name, 'Aardvark');
    assert.strictEqual(
      animals.some((animal) => animal.name === 'Rhino'),
      false,
    );
  });
});
