import { uniqBy, orderBy } from 'lodash-es';
import type { IDatabase } from 'pg-promise';
import chalk from 'chalk';
import DBModule, {
  migrationsTableExists,
  createMigrationsTable,
  getExecutedMigrations,
} from '../db/db.ts';
import { getFileMigrations } from '../files/files.ts';

interface Migration {
  fileName?: string;
  script?: string;
  newFileHash?: string;
  previousFileHash?: string;
  isExecuted?: boolean;
  isChanged?: boolean;
  isMissingScript?: boolean;
  isOnceOnly?: boolean;
  shouldExecute?: boolean;
  isValidFileName?: boolean;
  skip?: boolean;
}

interface MigrationParams {
  script?: string;
  isExecuted?: boolean;
  isChanged?: boolean;
  isOnceOnly?: boolean;
  force?: boolean;
  skip?: boolean;
}

export const shouldExecuteMigration = ({
  script,
  isExecuted,
  isChanged,
  isOnceOnly,
  force,
  skip,
}: MigrationParams): boolean => {
  const hasScript = typeof script === 'string'; // there must always be a script
  if (hasScript && !isExecuted && !skip) {
    // always execute migrations that have not yet been executed except for *.skip.sql scripts
    return true;
  } else if (hasScript && isChanged && force && !isOnceOnly && !skip) {
    // re-execute migrations that have changed, except for *.once.sql scripts and *.skip.sql scripts
    return true;
  } else {
    return false;
  }
};

export const showWarnings = (migrations: Migration[], force: boolean): void => {
  const invalidFileNames = migrations.filter(
    (m) => m.isValidFileName === false,
  );
  if (invalidFileNames.length) {
    const error =
      'Some filenames have invalid format (ISO 8601 without colons e.g. 2021-09-07T093045-some-description.sql)';
    invalidFileNames.forEach((s) => console.log(chalk.red(` ${s.fileName}`)));
    throw new Error(error);
  }
  const missingScripts = migrations.filter((m) => m.isMissingScript);
  if (missingScripts.length) {
    console.warn(
      chalk.yellow('Missing some expected (previously executed) scripts:'),
    );
    missingScripts.forEach((s) => console.log(chalk.yellow(` ${s.fileName}`)));
  }
  const illegalWords = ['COMMIT', 'ROLLBACK'];
  const illegalScripts = migrations.filter((m) =>
    illegalWords.some(
      (w) => typeof m.script === 'string' && m.script.toUpperCase().includes(w),
    ),
  );
  if (illegalScripts.length) {
    const error = `Migrations aborted. Illegal keywords found (one of ${illegalWords.join(
      ', ',
    )}).`;
    throw new Error(error);
  }
  const changedScripts = migrations.filter((m) => m.isChanged);
  if (changedScripts.length) {
    console.warn(
      chalk.yellow(
        'Some (previously executed) scripts have changed (prefer writing a new migration instead):',
      ),
    );
    changedScripts.forEach((s) => console.log(chalk.yellow(` ${s.fileName}`)));
    if (!force) {
      const error =
        'Migrations aborted. Run `npm run migrator:force` to ignore these warnings';
      throw new Error(error);
    }
  }
};

export const zipMigrations = async (
  fileMigrations: Migration[],
  executedMigrations: Migration[],
  force: boolean,
): Promise<Migration[]> => {
  const zippedMigrations = uniqBy(
    executedMigrations.concat(fileMigrations),
    'fileName',
  );
  const zippedMigrationsWithMetadata = zippedMigrations.map((m) => {
    const executedMigration = executedMigrations.find(
      (e) => e.fileName === m.fileName,
    );
    const isExecuted = executedMigration !== undefined;
    const fileMigration = fileMigrations.find((f) => f.fileName === m.fileName);
    const script = fileMigration && fileMigration.script;
    const newFileHash = fileMigration && fileMigration.newFileHash;
    const previousFileHash =
      executedMigration && executedMigration.previousFileHash;
    const isChanged =
      executedMigration && script && previousFileHash !== newFileHash;
    const isMissingScript = executedMigration && !fileMigration;
    const isOnceOnly = fileMigration && fileMigration.isOnceOnly;
    const skip = fileMigration && fileMigration.skip;
    const isValidFileName = fileMigration
      ? fileMigration.isValidFileName
      : true;
    const shouldExecute = shouldExecuteMigration({
      script,
      isExecuted,
      isChanged,
      isOnceOnly,
      force,
      skip,
    });
    return {
      fileName: m.fileName,
      isValidFileName,
      script,
      previousFileHash,
      newFileHash,
      isExecuted,
      isChanged,
      isMissingScript,
      isOnceOnly,
      shouldExecute,
    };
  });
  const orderedMigrations = orderBy(
    zippedMigrationsWithMetadata,
    (migration) => migration.fileName,
  );
  showWarnings(orderedMigrations, force);
  const migrationsToExecute = orderedMigrations.filter((m) => m.shouldExecute);
  if (!migrationsToExecute.length) {
    console.log(chalk.grey('No new migrations found'));
  }
  return migrationsToExecute;
};

export const migrate =
  (db: IDatabase<unknown>) =>
  (migrations: Migration[]): Promise<void> => {
    if (migrations.length) {
      console.log(chalk.gray('Executing migration scripts...'));
      return new Promise((resolve, reject) => {
        db.tx(async (t) => {
          for await (const migration of migrations) {
            console.log(chalk.gray(` ${migration.fileName}`));
            await t.any(migration.script);
          }
        })
          .then(() => {
            console.log(chalk.green('Completed migrations (success) ✓'));
            resolve();
          })
          .catch((error: Error) => {
            console.error(chalk.red('Migrations failed (rolled back) x'));
            reject(error);
          });
      });
    }
    return Promise.resolve();
  };

interface MigratorParams {
  dbConnection: string;
  dir: string;
  force?: boolean;
  autoInitializeTable?: boolean;
}

export const migrator = async ({
  dbConnection,
  dir,
  force,
  autoInitializeTable = true,
}: MigratorParams): Promise<void> => {
  const db = await DBModule.getDbConnection(dbConnection);
  if (autoInitializeTable) {
    const tableExists = await migrationsTableExists(db);
    if (!tableExists) {
      await createMigrationsTable(db);
    }
  }
  const executedMigrations = (await getExecutedMigrations(db)).map((m) => ({
    previousFileHash: m.file_hash,
    fileName: m.file_name,
  }));
  const fileMigrations = await getFileMigrations(dir);
  const migrations = await zipMigrations(
    fileMigrations,
    executedMigrations,
    force,
  );
  await migrate(db)(migrations);
};
