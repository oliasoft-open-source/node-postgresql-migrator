import { describe, mock, test } from 'node:test';
import assert from 'node:assert';
import {
  shouldExecuteMigration,
  showWarnings,
  zipMigrations,
} from './migrator.ts';

describe('Migrator', () => {
  test('getMigrations returns empty array when there are no migrations', async () => {
    const consoleSpy = mock.method(console, 'log');
    const fileMigrations = [];
    const databaseMigrations = [];
    const migrations = await zipMigrations(
      fileMigrations,
      databaseMigrations,
      false,
    );
    assert.strictEqual(migrations.length, 0);
    assert.strictEqual(consoleSpy.mock.callCount(), 1);
    assert.match(
      consoleSpy.mock.calls[0].arguments[0],
      /No new migrations found/,
    );
    consoleSpy.mock.restore();
  });

  test('getMigrations should get migrations with all metadata', async () => {
    const fileMigrations = [
      {
        fileName: '2021-09-07T154500-add-alpacas.sql',
        newFileHash:
          'e4226035dd9670bb0c944daf1cb8fea8955e8fcfd3ca9e65568930091f59508a',
        script: 'SOME SQL',
        isOnceOnly: false,
      },
    ];
    const databaseMigrations = [
      {
        fileHash:
          '26acffe99669b7e0dd34d2c341173fe9318783423e17955dbe67357731dae087',
        fileName: '2021-09-07T154500-add-giraffes.sql',
      },
    ];
    const force = false;
    const migrations = await zipMigrations(
      fileMigrations,
      databaseMigrations,
      force,
    );
    assert.strictEqual(migrations.length, 1);
    assert.strictEqual(migrations[0].script, 'SOME SQL');
    assert.strictEqual(migrations[0].previousFileHash, undefined);
    assert.strictEqual(
      migrations[0].newFileHash,
      'e4226035dd9670bb0c944daf1cb8fea8955e8fcfd3ca9e65568930091f59508a',
    );
    assert.strictEqual(migrations[0].isChanged, undefined);
    assert.strictEqual(migrations[0].isMissingScript, undefined);
    assert.strictEqual(migrations[0].isOnceOnly, false);
    assert.strictEqual(migrations[0].shouldExecute, true);
  });

  test('should handle when a script was previously executed', async () => {
    const fileMigrations = [
      {
        fileName: '2021-09-07T154500-add-alpacas.sql',
        newFileHash:
          'de6c84940a0f3a73dbf47f4088f823a35d0e1588f808f8945778077ea45fa489',
        script: 'SOME SQL',
        isOnceOnly: false,
      },
    ];
    const databaseMigrations = [
      {
        fileHash:
          '94eabbc97089eda4de93ed52ccf6c07b6a4b2209a4f375bd9db86646729729a4',
        fileName: '2021-09-07T154500-add-alpacas.sql',
      },
    ];
    const force = true;
    const migrations = await zipMigrations(
      fileMigrations,
      databaseMigrations,
      force,
    );
    assert.strictEqual(migrations.length, 1);
    assert.strictEqual(migrations[0].script, 'SOME SQL');
    assert.strictEqual(migrations[0].previousFileHash, undefined);
    assert.strictEqual(
      migrations[0].newFileHash,
      'de6c84940a0f3a73dbf47f4088f823a35d0e1588f808f8945778077ea45fa489',
    );
    assert.strictEqual(migrations[0].isChanged, true);
    assert.strictEqual(migrations[0].isMissingScript, false);
    assert.strictEqual(migrations[0].isOnceOnly, false);
    assert.strictEqual(migrations[0].shouldExecute, true);
  });

  test('shouldExecuteMigration should execute a script that has never been executed', () => {
    assert.strictEqual(
      shouldExecuteMigration({
        script: '',
        isExecuted: false,
        isChanged: false,
        isOnceOnly: false,
        force: false,
      }),
      true,
    );
  });

  test('shouldExecuteMigration should execute a *.once.sql script that has never been executed', () => {
    assert.strictEqual(
      shouldExecuteMigration({
        script: '',
        isExecuted: false,
        isChanged: false,
        isOnceOnly: true,
        force: false,
      }),
      true,
    );
  });

  test('shouldExecuteMigration should execute a changed, executed script with force applied', () => {
    assert.strictEqual(
      shouldExecuteMigration({
        script: '',
        isExecuted: true,
        isChanged: true,
        isOnceOnly: false,
        force: true,
      }),
      true,
    );
  });

  test('shouldExecuteMigration should not execute a migration missing metadata (safe default)', () => {
    assert.strictEqual(shouldExecuteMigration({}), false);
  });

  test('shouldExecuteMigration should not execute a *.once.sql script that has been executed and is forced', () => {
    assert.strictEqual(
      shouldExecuteMigration({
        script: '',
        isExecuted: true,
        isChanged: false,
        isOnceOnly: true,
        force: true,
      }),
      false,
    );
  });

  test('shouldExecuteMigration should not execute a changed *.once.sql script that has been executed and is forced', () => {
    assert.strictEqual(
      shouldExecuteMigration({
        script: '',
        isExecuted: true,
        isChanged: true,
        isOnceOnly: true,
        force: true,
      }),
      false,
    );
  });

  test('shouldExecuteMigration should not execute a *.skip.sql script ever', () => {
    assert.strictEqual(
      shouldExecuteMigration({
        script: '',
        isExecuted: true,
        isChanged: true,
        isOnceOnly: false,
        force: true,
        skip: true,
      }),
      false,
    );
  });

  test('shouldExecuteMigration should not execute a changed script that has been executed and is not forced', () => {
    assert.strictEqual(
      shouldExecuteMigration({
        script: '',
        isExecuted: true,
        isChanged: true,
        isOnceOnly: false,
        force: false,
      }),
      false,
    );
  });

  test('showWarnings should log error and exit upon invalid file names', () => {
    const migrations = [
      { fileName: '202-15-99T-bad--description.sql', isValidFileName: false },
    ];
    const force = true;
    assert.throws(() => {
      showWarnings(migrations, force);
    }, /Some filenames have invalid format/);
  });

  test('showWarnings should log warning when a previously executed script is missing', () => {
    const consoleSpy = mock.method(console, 'warn');
    const migrations = [{ isMissingScript: true }];
    const force = false;
    showWarnings(migrations, force);
    assert.strictEqual(consoleSpy.mock.callCount(), 1);
    assert.match(
      consoleSpy.mock.calls[0].arguments[0],
      /Missing some expected \(previously executed\) scripts/,
    );
    consoleSpy.mock.restore();
  });

  test('showWarnings should log error and exit upon illegal words in SQL', () => {
    const migrations = [{ script: 'COMMIT' }];
    const force = false;
    assert.throws(() => {
      showWarnings(migrations, force);
    }, /Illegal keywords found/);
  });

  test('showWarnings should log warning and exit when a SQL script has changed', () => {
    const consoleSpy = mock.method(console, 'warn');
    const migrations = [{ isChanged: true }];
    const force = false;
    assert.throws(() => {
      showWarnings(migrations, force);
    });
    assert.strictEqual(consoleSpy.mock.callCount(), 1);
    assert.match(
      consoleSpy.mock.calls[0].arguments[0],
      /Some \(previously executed\) scripts have changed/,
    );
    consoleSpy.mock.restore();
    consoleSpy.mock.restore();
  });

  test('showWarnings should log warning and not exit when a SQL script has changed and force is true', () => {
    const migrations = [{ isChanged: true }];
    const force = true;
    assert.doesNotThrow(() => {
      showWarnings(migrations, force);
    });
  });
});
