import pgp, { IDatabase } from 'pg-promise';

const getDbConnection = async (databaseConfig: string) => {
  const pgOptions = {};
  const pgPromise = pgp(pgOptions);
  return pgPromise(databaseConfig);
};

export const migrationsTableExists = (db: IDatabase<unknown>) => {
  const query = `
    SELECT EXISTS (
      SELECT FROM information_schema.tables
      WHERE table_schema = 'public'
        AND table_name = 'migrations'
    )
  `;
  return db.any(query);
};

export const createMigrationsTable = (db: IDatabase<unknown>) => {
  const query = `
    CREATE TABLE IF NOT EXISTS migrations (
      file_hash TEXT NOT NULL PRIMARY KEY,
      file_name TEXT NOT NULL,
      created_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
    )
  `;
  return db.any(query);
};

export const getExecutedMigrations = (db: IDatabase<unknown>) => {
  const query = `
    SELECT DISTINCT ON (file_name)
      file_hash,
      file_name
    FROM migrations
    ORDER BY
      file_name ASC,
      created_on DESC
   `;
  return db.any(query);
};

/*
  Default export allows mocking of db connection tests https://github.com/nodejs/help/issues/4295#issuecomment-1917010838
  Otherwise I would make getDbConnection a named export
*/
const DBModule = { getDbConnection };
export default DBModule;
