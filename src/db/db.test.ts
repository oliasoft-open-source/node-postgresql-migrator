import { describe, test } from 'node:test';
import assert from 'node:assert';
import { isFunction } from 'lodash-es';
import { newDb } from 'pg-mem';
import DBModule, {
  createMigrationsTable,
  getExecutedMigrations,
} from './db.ts';

const setupEmptyDb = async () => {
  const db = await newDb().adapters.createPgPromise();
  db.connect();
  return db;
};

const setupDb = async () => {
  const db = await newDb().adapters.createPgPromise();
  db.connect();
  await createMigrationsTable(db);
  return db;
};

describe('Database', () => {
  test('getDbConnection returns a DB connection object', async () => {
    const db = await DBModule.getDbConnection(
      'postgres://username:password@host:port/db',
    );
    assert.strictEqual(isFunction(db.connect), true);
  });

  test('createMigrationsTable creates the migrations table', async () => {
    const db = await setupEmptyDb();
    await createMigrationsTable(db);
    const migrations = await db.one('SELECT COUNT(*) FROM migrations');
    assert.strictEqual(migrations.count, 0);
  });

  test('getExecutedMigrations returns as empty initially', async () => {
    const db = await setupDb();
    const executedMigrations = await getExecutedMigrations(db);
    assert.strictEqual(executedMigrations.length, 0);
  });

  test('getExecutedMigrations returns migrations from table', async () => {
    const db = await setupDb();
    await db.any(`
    INSERT INTO migrations (file_hash, file_name)
    VALUES (
    'cf02fc9ca782f5f255e70af78dd77c75f69d59922d13b1b0504cbfc19b101362',
    '2021-09-08T202100-wombats.sql')
  `);
    const executedMigrations = await getExecutedMigrations(db);
    assert.strictEqual(executedMigrations.length, 1);
    assert.strictEqual(
      executedMigrations[0].file_name,
      '2021-09-08T202100-wombats.sql',
    );
  });

  test('getExecutedMigrations returns distinct by name, sorted by created_on', async () => {
    const db = await setupDb();
    await db.any(`
    INSERT INTO migrations (file_hash, file_name, created_on)
    VALUES (
    '779a14c5885bdde0458b8da9736e61fb1d021b11f94f4db678b56e74ae49a405',
    '2021-09-08T202100-wombats.sql',
    '2021-09-09 09:00:00-00' --latest timestamp wins when duplicates
    ), (
    'b641579ecbcc9c07f2620efcbfd300d73c05d0521280d42eaba0ba3484405127',
    '2021-09-08T202100-wombats.sql',
     '2020-03-09 09:00:00-00'
    ), (
    '859cab744a2546143c28971cc008cd19233bd5d8a712f122c703945d69b16afd',
    '2021-09-08T202100-squirrels.sql',
    '2021-09-08 09:00:00-00'
    ), (
    'd091389f93b9adc4cf11caee1fe26c3e00bb784e8070fef5ae7c9365d0240230',
    '2021-09-08T202100-wombats.sql',
    '2021-09-04 09:00:00-00'
    )
  `);
    const executedMigrations = await getExecutedMigrations(db);
    assert.strictEqual(executedMigrations.length, 2);
    assert.strictEqual(
      executedMigrations[0].file_hash,
      '859cab744a2546143c28971cc008cd19233bd5d8a712f122c703945d69b16afd',
    );
    assert.strictEqual(
      executedMigrations[1].file_hash,
      '779a14c5885bdde0458b8da9736e61fb1d021b11f94f4db678b56e74ae49a405',
    );
  });
});
