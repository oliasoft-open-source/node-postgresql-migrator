import { spawn } from 'child_process';
import { test, describe } from 'node:test';
import assert from 'node:assert';

// Spawn a child process to invoke the SQL migrator utility
const runMigrator = async (
  command: string,
): Promise<{ stdout: string; stderr: string; code: number | null }> => {
  return new Promise((resolve) => {
    let stdout = '',
      stderr = '';
    const child = spawn(command, [], { shell: true });
    child.stdout.on(
      'data',
      (data) => (stdout = stdout.concat(data.toString())),
    );
    child.stderr.on(
      'data',
      (data) => (stderr = stderr.concat(data.toString())),
    );
    child.on('exit', (code) => resolve({ stdout, stderr, code }));
  });
};

describe('Integration tests', () => {
  describe('CLI', () => {
    test('can invoke the dev cli', async () => {
      const { stderr, code } = await runMigrator('npx tsx src/cli.js');
      assert.strictEqual(code, 1);
      assert.strictEqual(stderr.includes('Missing required arguments'), true);
    });
    test('can invoke the prod cli', async () => {
      const { stderr, code } = await runMigrator('node ./dist/cli.js');
      assert.strictEqual(code, 1);
      assert.strictEqual(stderr.includes('Missing required arguments'), true);
    });
  });
});
